
# Aquatic Quality Analysis

Aquatic Quality Analysis is a project that I completed in my final semester of my undergraduate studies. The goal of this project was to use machine learning techniques to classify water samples as drinkable or not drinkable based on nine chemical parameters. I used a publicly available [Dataset](https://www.kaggle.com/datasets/adityakadiwal/water-potability) from [Kaggle](https://www.kaggle.com/datasets) that contained measurements of these parameters for different water sources. I applied various classification algorithms and evaluated their performance using accuracy, precision, recall and F1-score metrics. This project helped me to learn more about the applications of machine learning in environmental science and the challenges of data preprocessing, feature selection and model tuning.


## Abstract
Aquatic quality defines the suitability of water for
certain purposes. Assessing the quality of aquatic environments
is crucial for effective water resource management. Our research
focuses on analyzing the chemical, physical, and biological characteristics that influence aquatic quality. Our study looks forward
to classifying the suitability of water for human usage. To achieve
this purpose, we start by conducting exploratory data analysis
(EDA) to analyze datasets to get prepared to train. Then we
preprocess the data to check its suitability for model training. We
evaluate the performance of various models, including Logistic
Regression, K Nearest Neighbors (KNN), and Random Forest,
using preprocessed dataset. Finally, we implement a confusion
matrix to measure the accuracy and effectiveness of each model
in classifying water quality. Through this work, our goal is to
provide the best usage of water for humans.
## Dataset
    1. ph: pH of 1. water (0 to 14).
    2. Hardness: Capacity of water to precipitate soap in mg/L.
    3. Solids: Total dissolved solids in ppm.
    4. Chloramines: Amount of Chloramines in ppm.
    5. Sulfate: Amount of Sulfates dissolved in mg/L.
    6. Conductivity: Electrical conductivity of water in μS/cm.
    7. Organic_carbon: Amount of organic carbon in ppm. 
    8. Trihalomethanes: Amount of Trihalomethanes in μg/L.
    9. Turbidity: Measure of light emiting property of water in NTU.
    10. Potability: Indicates if water is safe for human consumption. Potable -1 and Not potable -0

## Tech Used

**Language** Python

**Development Tool** Visual Studio Code, Jupyter Notebook

**Platform** [Google Colab](https://colab.research.google.com/)


## Library
Here are the libraries used in code:

- Seaborn
- Plotly.express
- Matplotlib.pyplot
- Plotly.graph_objects
- Plotly.figure_factory
- NumPy
- Pandas
- Random
- Sklearn.impute.KNNImputer
- Sklearn.preprocessing.StandardScaler
- Sklearn.model_selection.train_test_split
- Sklearn.linear_model.LogisticRegression
- Sklearn.metrics.confusion_matrix
- Sklearn.neighbors.KNeighborsClassifier
- Sklearn.svm
- Xgboost.XGBClassifier
- Sklearn.tree.DecisionTreeClassifier
- Sklearn.ensemble.RandomForestClassifier
- Sklearn.metrics.accuracy_score
- Sklearn.metrics.classification_report
- Sklearn.metrics.f1_score
## Feedback

If you have any feedback, please reach out to us at http://scr.im/84ta

